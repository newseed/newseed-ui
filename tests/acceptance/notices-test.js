import { module, test } from 'qunit';
import { click, visit, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';
import { truncateText } from 'newseed/helpers/truncate-text';

module('Acceptance | notices', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  hooks.beforeEach(function() {
    this.server.db.emptyData();
    this.server.loadFixtures();
  });

  test('Notices list page', async function(assert) {
    const notices = this.server.createList('post', 10, 'news');

    await visit('/news');

    assert.equal(currentURL(), '/news');

    assert.dom('[data-test-notices-title]').hasText('Notices');
    assert.dom('[data-test-notice]').exists({ count: notices.length });

    notices.forEach(notice => {
      assert
        .dom(`[data-test-notice='${notice.id}'] [data-test-notice-link]`)
        .hasText(notice.title);

      assert
        .dom(`[data-test-notice='${notice.id}'] [data-test-notice-type]`)
        .hasText(notice.type);

      assert.dom(`[data-test-notice='${notice.id}'] [data-test-notice-link-image]`).exists();
      assert
        .dom(`[data-test-notice='${notice.id}'] [data-test-notice-image]`)
        .hasAttribute('src', notice.image);
      assert
        .dom(`[data-test-notice='${notice.id}'] [data-test-notice-image]`)
        .hasAttribute('alt', notice.title);

      assert
        .dom(`[data-test-notice='${notice.id}'] [data-test-notice-text]`)
        .hasText(truncateText([notice.text], { limit: 600, stripHtml: true }));

      notice.tags.forEach(tag => {
        assert
          .dom(`[data-test-notice='${notice.id}'] [data-test-notice-link-tag=${tag.name}]`)
          .hasText(tag.name);
      });
    });
  });

  test('Notices list go to notice page', async function(assert) {
    const notices = this.server.createList('post', 10, 'news');
    const notice = notices[0];

    await visit('/news');
    await click(`[data-test-notice='${notice.id}'] [data-test-notice-link]`);

    assert.equal(currentURL(), `/news/${notice.id}`);
  });

  test('Notice page', async function(assert) {
    const notices = this.server.createList('post', 10, 'news');
    const notice = notices[0];

    await visit(`/news/${notice.id}`);
    assert.equal(currentURL(), `/news/${notice.id}`);

    assert.dom('[data-test-link-notices]').exists();
    assert.dom('[data-test-notice-title]').hasText(notice.title);
    assert.dom('[data-test-notice-text]').hasText(notice.text);
    assert.dom('[data-test-notice-image]').hasAttribute('src', notice.image);
    assert.dom('[data-test-notice-image]').hasAttribute('alt', notice.title);
  });

  test('Notice page go to the notices list', async function(assert) {
    const notices = this.server.createList('post', 10, 'news');
    const notice = notices[0];

    await visit(`/news/${notice.id}`);
    assert.equal(currentURL(), `/news/${notice.id}`);

    await click('[data-test-link-notices]');
    assert.equal(currentURL(), '/news');
  });
});
