import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';
import SetChurchController from 'newseed/mixins/org-set-controller';

export default Route.extend(SetChurchController, UnauthenticatedRouteMixin, {
  session: service(),
  beforeModel() {
    if (this.get('session.isAuthenticated')) {
      this.transitionTo('admin.content');
    }
  }
});
