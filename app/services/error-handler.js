import Service from '@ember/service';

export default Service.extend({
  returnMessages(error) {
    const errorMessageArray = error.errors.map(err => {
        return `${err.status}: ${err.title}`;
    })
    return errorMessageArray.join(' | ');
  }
});