import Mixin from '@ember/object/mixin';
import config from 'newseed/config/environment';
import EmberObject from '@ember/object';

export default Mixin.create({
  setupController(controller, model) {
    this._super(controller, model);
    const types = EmberObject.create({ posts: false, pages: false, sermons: false });
    controller.set('display', types);
    this.store.findRecord('org', config.org).then(function (org) {
      controller.set('org', org);
    });  
    this.store.query('post', { status: 'published' })
      .then(function (posts) {
        const pages = posts.filter(p => p.get('type') === 'page');
        controller.set('pages', pages);
        posts.forEach(p => {
          switch (p.get('type')) {
            case 'ministry':
              controller.set('display.ministries', true);
              break
            case 'news':
              controller.set('display.posts', true);
              break;
            case 'event':
              controller.set('display.posts', true);
              break;
            case 'need':
              controller.set('display.posts', true);
              break;
            default:
              break;
          }
        });
      });
    this.store.query('sermon', { status: 'published' })
      .then(function (sermons) {
        if (sermons.length) controller.set('display.sermons', true);
      });
  }
});
