import Component from '@ember/component';

export default Component.extend({
    passwordView: false,
    actions: {
        togglePassword() {
            this.toggleProperty('passwordView');
        }
    }
});
