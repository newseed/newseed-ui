import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import SetChurchController from 'newseed/mixins/org-set-controller';

export default Route.extend(AuthenticatedRouteMixin, SetChurchController, {
  model() {
    return this.store.findAll('meeting');
  },
  actions: {
    refreshRoute() {
      this.refresh();
    }
  }
});
