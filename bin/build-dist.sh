#!/usr/bin/env bash

cd ~/Sites/newseed-ui
echo 'Building ember for' $1
ember build --environment=$1
echo 'Removing public folder from express'
rm -rf ~/Sites/newseed-api/public
mkdir ~/Sites/newseed-api/public
echo 'Moving files to express'
mv ./dist/* ~/Sites/newseed-api/public
echo 'Removing dist folder'
rm -rf ./dist
