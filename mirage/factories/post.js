import { Factory, trait } from 'ember-cli-mirage';
import faker from 'faker';
import moment from 'moment';

export default Factory.extend({
  type: () => faker.random.arrayElement(['news', 'need', 'ministry']),
  title: () => faker.lorem.words(),
  date: () => faker.date.between(moment().startOf('week'), moment().endOf('week')),
  text: () => faker.lorem.paragraph(),
  image: () => faker.random.image(),
  media: () => faker.random.image(),
  author: () => faker.name.findName(),
  expiry: () => faker.date.recent(-60),
  tags: () => [{ name: faker.lorem.words() }, { name: faker.lorem.word() }, { name: faker.lorem.word() }],
  status: 'published',
  frequency: 'week',
  slug: 'ut-fuga-animi-aut-impedit-1532899416499',

  slide: trait({
    tags: () => [{ name: 'slide' }]
  }),

  event: trait({
    type: 'event',
    tags: () => [{ name: 'event' }]
  }),

  ministry: trait({
    type: 'ministry',
    tags: () => [{ name: 'ministry' }]
  }),

  page: trait({
    type: 'page',
    tags: () => [{ name: 'page' }]
  }),

  news: trait({
    type: 'news',
    tags: () => [{ name: 'news' }]
  })
});
