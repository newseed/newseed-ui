import Component from '@ember/component';
import EmberObject, {computed} from '@ember/object';
import { A } from '@ember/array';
import moment from 'moment';
import populateCalendar from '../utils/populate-calendar';
import _ from 'lodash';

export default Component.extend({
  calendar: populateCalendar.create(),
  today: computed(function() {
    return moment().format('dddd');
  }),
  weekSchedule: computed('events', 'meetings', 'org', function() {
    let schedule = EmberObject.create(),
      events = this.get('events'),
      today = moment().startOf('day'),
      // set the checkDate to next Sunday just before midnight
      sunday = moment().day(7).endOf('day'),
      meetings = this.get('meetings').filter(meeting => {
        meeting.set('type', 'meeting');
        return moment(meeting.get('date')).isBetween(today, sunday);
      });

    // If there's no meetings setup then we need to take the org times
    if (!meetings.length && this.get('org')) {
      let timeArr = this.get('org.meetingTimes');
      // We use the array to push meeting time objects to meetings
      timeArr.forEach(t => {
        const meetingDay = this.get('org.meetingDay');
        const day = meetingDay ? moment(moment().day(meetingDay)).weekday() : 7;
        // Remove letters and split by seperator
        let hm = t.replace(/[^0-9:,.|-]+/g, '').split(new RegExp(/[:,.|-]/g));
        let time = moment().set({ day, hour:hm[0], minute:hm[1], second:0});
        let meeting = EmberObject.create({type: 'meeting', date: time });
        meetings.pushObject(meeting);
      })
    }

    schedule.setProperties({
      'Monday': A(),
      'Tuesday': A(),
      'Wednesday': A(),
      'Thursday': A(),
      'Friday': A(),
      'Saturday': A(),
      'Sunday': A()
    });

    this.get('calendar').setData(events, schedule);
    this.get('calendar').setData(meetings, schedule);

    const valid = _.filter(schedule, a => a.length );
    return valid.length ? schedule : null;
  }),
});
