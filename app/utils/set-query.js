/**
 * Build query for posts
 * @param {[object]} defaultQuery  The default query to build on eg. { type: "message" }
 * @param {[object]} params Any params to add to the default query
 */
export const setQuery = (query, params) => {
  Object.keys(params).forEach((key) => {
    if (params[key]) {
      query[key] = params[key];
    }
  });
  return query;
}
