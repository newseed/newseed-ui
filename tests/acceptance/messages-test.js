import { module, test } from 'qunit';
import { visit, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';
import { truncateText } from 'newseed/helpers/truncate-text';
import moment from 'moment';

module('Acceptance | messages', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  hooks.beforeEach(function() {
    this.server.db.emptyData();
    this.server.loadFixtures();
  });

  test('Messages list page', async function(assert) {
    const sermons = this.server.createList('sermon', 10);

    await visit('/messages');

    assert.equal(currentURL(), '/messages');

    assert.dom('[data-test-messages-title]').hasText('Messages');
    assert.dom('[data-test-messages-subtitle]').exists();
    assert.dom('[data-test-message]').exists({ count: sermons.length });

    sermons.forEach(sermon => {
      assert
        .dom(`[data-test-message='${sermon.id}'] [data-test-message-title]`)
        .includesText(sermon.title);

      assert
        .dom(`[data-test-message='${sermon.id}'] [data-test-message-text]`)
        .hasText(truncateText([sermon.text], { limit: 500, stripHtml: true }));

      assert
        .dom(`[data-test-message='${sermon.id}'] [data-test-message-link-speaker]`)
        .hasText(sermon.speaker);

      assert
        .dom(`[data-test-message='${sermon.id}'] [data-test-message-date]`)
        .includesText(moment(sermon.date).format('D MMM YY'));

      sermon.tags.forEach(tag => {
        assert
          .dom(`[data-test-message='${sermon.id}'] [data-test-message-link-tag=${tag.name}]`)
          .hasText(tag.name);
      });

      assert
        .dom(`[data-test-message='${sermon.id}'] [data-test-message-link-download]`)
        .hasAttribute('href', sermon.fileUrl);
    });
  });
});
