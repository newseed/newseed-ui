import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  store: service(),
  router: service(),

  actions: {
    savePersonDetail(fullName, email) {
      let user = this.get('user');
      this.store.findRecord('user', user.id)
        .then(record => {
          record.setProperties({ fullName, email });
          record.save();
          this.get('flashMessages').success('Person saved ✝☦✝');
        })
        .catch(() => {
          this.get('flashMessages').danger('Something went wrong - person not saved');
        });
    },
    savePersonPassword(password) {
      let user = this.get('user');
      this.store.findRecord('user', user.id)
        .then(record => {
          record.setProperties({ password });
          record.save();
          this.get('flashMessages').success(`Password updated for ${user.fullName}`);
        })
        .catch(() => {
          this.get('flashMessages').danger('Something went wrong - person not saved');
        });
    },
    deletePeople() {
      let user = this.get('user');
      this.store.findRecord('people', user.id, { backgroundReload: false })
        .then(record => {
          record.destroyRecord().then(() => {
            this.get('flashMessages').success('Person successfully deleted');
          })
        })
        .catch(() => {
          this.get('flashMessages').danger('Something went wrong - person not deleted');
        });
    }
  }
});
