import Route from '@ember/routing/route';
// import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';
import SetChurchController from 'newseed/mixins/org-set-controller';

export default Route.extend(SetChurchController, {
    model() {
        return this.store.createRecord('user')
    }
});
