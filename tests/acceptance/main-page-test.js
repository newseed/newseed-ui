import { module, test } from 'qunit';
import { visit } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';
import { get } from '@ember/object';
import moment from 'moment';
import { truncateText } from 'newseed/helpers/truncate-text';

module('Acceptance | main-page', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  hooks.beforeEach(function() {
    this.server.db.emptyData();
    this.server.loadFixtures();
  });

  test('Slides carousel block', async function(assert) {
    const slides = this.server.createList('post', 5, 'slide');

    await visit('/');

    assert.dom('[data-test-carousel-indicator]').exists({ count: 5 });
    assert.dom('[data-test-carousel-item]').exists({ count: 5 });

    slides.forEach(slide => {
      assert
        .dom(`[data-test-carousel-item='${slide.id}'] [data-test-slide-link]`)
        .exists({ count: 1 });
      assert
        .dom(`[data-test-carousel-item='${slide.id}'] [data-test-slide-title]`)
        .hasText(slide.title);
      if (slide.type) {
        assert
          .dom(`[data-test-carousel-item='${slide.id}'] [data-test-slide-type]`)
          .hasText(slide.type);
      }
      assert
        .dom(`[data-test-carousel-item='${slide.id}'] [data-test-slide-image]`)
        .hasAttribute('src', slide.image);
    });
  });

  test('This sunday block without meetings', async function(assert) {
    this.server.createList('post', 2, 'event');

    await visit('/');

    assert
      .dom('[data-test-next-meeting-date]')
      .hasText(moment('9:00', 'h:mm').day(7).format('dddd, D MMM | h:mmA')); // from org fixture

    assert.dom('[data-test-next-meeting-title]').doesNotExist();
    assert.dom('[data-test-next-meeting-speaker]').doesNotExist();
  });

  test('This sunday block with meetings', async function(assert) {
    this.server.createList('post', 2, 'event');

    const meetings = [];

    for (let i = 0; i < 5; i++) {
      const meeting = this.server.create('meeting', {
        date: moment().add(1, 'hour').add(i, 'day')
      });
      meetings.push(meeting);
    }

    const nextMeeting = meetings[0];

    await visit('/');

    assert
      .dom('[data-test-next-meeting-date]')
      .hasText(`${moment(nextMeeting.date).format('dddd')}, ${moment(nextMeeting.date).format('D MMM')} | ${moment(nextMeeting.date).format('h:mmA')}`);

    assert.dom('[data-test-next-meeting-title]').hasText(nextMeeting.title);

    assert.dom('[data-test-next-meeting-speaker]').hasText(nextMeeting.speaker);
  });

  test('This week block', async function(assert) {
    const events = this.server.createList('post', 5, 'event');
    const meetings = this.server.createList('meeting', 5);

    await visit('/');

    assert.dom('[data-test-this-week-schedule] [data-test-meeting]').exists({ count: meetings.length });
    assert.dom('[data-test-this-week-schedule] [data-test-event]').exists({ count: events.length });

    events.forEach(event => {
      const eventDay = moment(event.date).format('dddd');
      assert
        .dom(`[data-test-this-week-schedule] [data-test-day=${eventDay}] [data-test-event='${event.id}']`)
        .hasText(`${moment(event.date).format('h:mmA')} - ${event.title}`);
    });

    meetings.forEach(meeting => {
      const meetingDay = moment(meeting.date).format('dddd');
      assert
        .dom(`[data-test-this-week-schedule] [data-test-day=${meetingDay}] [data-test-meeting='${meeting.id}']`)
        .hasText(`${moment(meeting.date).format('h:mmA')} - Service`);
    });
  });

  test('Notices block', async function(assert) {
    const notices = this.server.createList('post', 10, 'event');

    const filteredNotices = notices
      .sort((a, b) => {
        return moment(get(a, 'date')).isBefore(moment(get(b, 'date'))) ? 1 : -1;
      })
      .slice(0, 7);

    await visit('/');

    assert.dom('[data-test-notices]').exists();
    assert.dom('[data-test-notices] [data-test-link-posts]').exists();

    filteredNotices.forEach(notice => {
      assert
        .dom(`[data-test-notices] [data-test-post='${notice.id}'] [data-test-post-link]`)
        .hasText(notice.title);

      assert
        .dom(`[data-test-notices] [data-test-post='${notice.id}'] [data-test-post-type]`)
        .hasText(notice.type);

      assert
        .dom(`[data-test-notices] [data-test-post='${notice.id}'] [data-test-post-text]`)
        .hasText(truncateText([notice.text], { limit: 154, stripHtml: true }));

      assert
        .dom(`[data-test-notices] [data-test-post='${notice.id}'] [data-test-post-date]`)
        .includesText(moment(notice.date).format('D MMM YY'));

      notice.tags.forEach(tag => {
        assert
        .dom(`[data-test-notices] [data-test-post='${notice.id}'] [data-test-post-tag-link]`)
        .hasText(tag.name);
      });
    });
  });

  test('Messages block', async function(assert) {
    this.server.createList('post', 2, 'event');
    const sermons = this.server.createList('sermon', 10);

    const filteredSermons = sermons
      .sort((a, b) => {
        return moment(get(a ,'date')).isBefore(moment(get(b, 'date'))) ? 1 : -1;
      })
      .slice(0, 5);

    await visit('/');

    assert.dom('[data-test-media]').exists();
    assert.dom('[data-test-media] [data-test-link-messages]').exists();
    for (const [index, sermon] of filteredSermons.entries()) {
      assert
        .dom(`[data-test-media] [data-test-media-item='${sermon.id}'] [data-test-media-title]`)
        .includesText(sermon.title);

      assert
        .dom(`[data-test-media] [data-test-media-item='${sermon.id}'] [data-test-link-messages-speaker]`)
        .hasText(sermon.speaker);

      sermon.tags.forEach(tag => {
        assert
          .dom(`[data-test-media] [data-test-media-item='${sermon.id}'] [data-test-link-messages-tag=${tag.name}]`)
          .hasText(tag.name);
      });

      assert
        .dom(`[data-test-media] [data-test-media-item='${sermon.id}'] [data-test-media-date]`)
        .includesText(moment(sermon.date).format('D MMM YY'));

      assert
        .dom(`[data-test-media] [data-test-media-item='${sermon.id}'] [data-test-media-text]`)
        .hasText(truncateText([sermon.text], { limit: 230, stripHtml: true }));

      if (index === 0) {
        assert
          .dom(`[data-test-media] [data-test-media-item='${sermon.id}'] [data-test-media-video]`).isVisible();
      } else {
        assert
          .dom(`[data-test-media] [data-test-media-item='${sermon.id}'] [data-test-media-video]`).isNotVisible();
      }

      assert
        .dom(`[data-test-media] [data-test-media-item='${sermon.id}'] [data-test-media-download-link]`)
        .hasAttribute('href', sermon.fileUrl);
    }
  });

  test('Address map block', async function(assert) {
    const org = this.server.schema.orgs.first();

    await visit('/');

    assert.dom('[data-test-address-map]').exists();
    assert.dom('[data-test-address-map] [data-test-org-name]').hasText(org.name);
    assert.dom('[data-test-address-map] [data-test-org-address]').hasText(org.address);
    assert.dom('[data-test-address-map] [data-test-org-address2]').hasText(org.address2);
    assert.dom('[data-test-address-map] [data-test-org-city]').hasText(org.city);
    assert.dom('[data-test-address-map] [data-test-org-state]').hasText(org.state);
    assert.dom('[data-test-address-map] [data-test-org-phone]').hasText(org.phone);
    assert.dom('[data-test-address-map] [data-test-org-meeting-times]').hasText(`${org.meetingDay} - ${org.meetingTimes}`);
  });
});
