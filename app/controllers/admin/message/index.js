import { inject as service } from '@ember/service';
import Controller from '@ember/controller';
import { get } from '@ember/object';
import { sort } from '@ember/object/computed';
import moment from 'moment';

export default Controller.extend({
  session: service(),
  errorHandler: service(),

  sortedMedia: sort('model', function (a, b) {
    return moment(get(a, 'date')).isBefore(moment(get(b, 'date'))) ? 1 : -1;
  }),

  actions: {
    createContent() {
      let record = this.store.createRecord('sermon', {
        title: 'New Message Content'
      });      
      record.save()
        .then(rec => {
          let id = rec.get('id');
          this.transitionToRoute('admin.message.edit', id);
        })
        .catch(err => {
          record.deleteRecord();          
          this.get('flashMessages').danger(this.errorHandler.returnMessages(err));
        });
    }
  }
});
