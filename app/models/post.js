import DS from 'ember-data';
import moment from 'moment';

const d = new Date();

export default DS.Model.extend({
  title: DS.attr('string'),
  slug: DS.attr('string'),
  author: DS.attr(),
  type: DS.attr('string', { defaultValue: 'news'}),
  text: DS.attr('string', { defaultValue: ''}),
  image: DS.attr('string'),
  date: DS.attr('date', {
    defaultValue() {
      return d;
    }
  }),
  expiry: DS.attr('date', {
    defaultValue() {
      let addMonth = moment(d).add(1, 'M').toDate();
      return addMonth;
    }
  }),
  frequency: DS.attr('string', { defaultValue: null}),
  tags: DS.attr(),
  updated_at: DS.attr('date'),
  created_at: DS.attr('date'),
  status: DS.attr('string', { defaultValue: 'draft'}),
});
