'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'newseed',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },

    apiHost: 'http://localhost:8081',
    apiNameSpace: 'api/v1',

    org: 'e1785e82-13d4-4021-851c-d96e93e5471d',
    header: {
      title: 'The OLIVE TREE Church | Petone, Lower Hutt',
      description: 'Tēnā koutou katoa. The Olive Tree is a small church where everyone is welcome. You can find us at the end of Jackson Street in Petone.',
      'google-site-verification': 'hT467oZSkScs0rRMCt-hO9ruAm9RmKpbKzQ5ZUQyj68'
    },

    contentSecurityPolicy: {
     'style-src': "'self' 'unsafe-inline'"
    },

    torii: {
      allowUnsafeRedirects: true,
      providers: {
        'facebook-oauth2': {
          apiKey: '631252926924840'
        }
      }
    },

    flashMessageDefaults: {
      timeout: 3000,
      priority: 200,
      preventDuplicates: true
    },

    // googleClientID: '694766332436-1g5bakjoo5flkfpv3t2mfsch9ghg7ggd.apps.googleusercontent.com',

    metricsAdapters: [
      {
        name: 'GoogleAnalytics',
        environments: ['production'],
        config: {
          id: 'UA-27428401-1',
          // // Use `analytics_debug.js` in development
          // debug: environment === 'development',
          // // Use verbose tracing of GA events
          // trace: environment === 'development',
          // // Ensure development env hits aren't sent to GA
          // sendHitTask: environment !== 'development',
          // // Specify Google Analytics plugins
          // require: ['ecommerce']
        }
      }
    ],

    moment: {
      includeTimezone: 'all'
    }
  };

  ENV['ember-simple-auth'] = {
    authenticationRoute: 'login',
    routeAfterAuthentication: 'admin.index',
    routeIfAlreadyAuthenticated: 'admin.index'
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV['ember-cli-mirage'] = {
      enabled: false
    };
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;

    ENV['ember-cli-mirage'] = {
      enabled: true
    };
  }

  if (environment === 'staging') {
    ENV.apiHost = 'https://newseed-staging.herokuapp.com';
    ENV.org = 'e1785e82-13d4-4021-851c-d96e93e5471d';
  }

  if (environment === 'production') {
    ENV.apiHost = 'https://www.olivetree.org.nz';
    ENV.org = 'e1785e82-13d4-4021-851c-d96e93e5471d';
  }

  return ENV;
};
