import { computed } from '@ember/object';
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { A } from '@ember/array';
// import config from 'newseed/config/environment';

export default Controller.extend({
  router: service(),
  stockImages: service(),
  session: service(),
  ajax: service(),

  init() {
    this._super(...arguments);
    this.set('isSaving', false);
  },

  formatTags: computed('model.tags', function() {
    return this.get('model.tags').map(tag => tag.name);
  }),

  actions: {
    titleEdit() {
      this.set('editTitle', true);
    },
    saveDate(date) {
      this.set('model.date', new Date(date));
      this.send('saveContent');
    },
    saveStatus(value) {
      this.set('model.status', value);
      this.send('saveContent');
    },
    saveSpeaker(value) {
      this.set('model.speaker', value);
      this.send('saveContent');
    },
    selectImage(image) {
      this.set('model.image', image);
      this.send('saveContent');
    },
    saveContent(resetFileInput) {
      let model = this.get('model');
      this.set('isSaving', true);
      if (this.get('editTitle')) {
        let title = this.get('model.title');
        model.set('title', title);
        this.set('editTitle', false);
      }
      model
        .save()
        .then(() => {
          if (typeof resetFileInput === 'function') {
            resetFileInput();
          }
          this.set('isSaving', false);
          this.get('flashMessages').success('Content successfully saved');
        })
        .catch(() => {
          this.set('isSaving', false);
          this.get('flashMessages').danger(
            'Something went wrong - content not saved',
          );
        });
    },
    deleteContent() {
      let model = this.get('model');
      model
        .destroyRecord()
        .then(() => {
          this.get('router').transitionTo('admin.message');
        })
        .catch(() => {
          this.get('flashMessages').danger(
            'Something went wrong - content not deleted',
          );
        });
    },
    uploadFile(file) {
      const model = this.get('model');
      const fileName = file.blob.name;
      let fileLocation;

      this.get('ajax')
        .request(`/sermons/upload?folder=sermons&fileName=${fileName}`, {
          method: 'GET',
        })
        .then(response => {
          const url = response.url;
          // TODO: find a less hacky way of uploading to aws using the direct CNAME
          fileLocation = url
            .split('?')[0] // get rid of aws keys and junk
            // .replace(/s3\.us-west-2\.amazonaws\.com\//, '') // remove s3 portion of url
            .replace(/^(https):\/\//, ''); // remove https            

          return file.upload({
            url,
            method: 'PUT',
            headers: {
              'Content-Type': file.blob.type,
            },
          });
        })
        .then(() => {
          model.set('fileUrl', fileLocation);
          this.send('saveContent');
        })
        .catch(error => {
          this.get('flashMessages').danger(`${error}`);
        });
    },
    addTag(tag) {
      const fullTag = { name: tag };
      if (!this.get('model.tags')) {
        this.set('model.tags', A());
      }
      this.get('model.tags').pushObject(fullTag);
      this.send('saveContent');
    },
    removeTagAtIndex(index) {
      this.get('model.tags').removeAt(index);
      this.send('saveContent');
    },
  },
});
