import Controller from '@ember/controller';

export default Controller.extend({
  queryParams: ['speaker', 'tags'],
  speaker: null,
  tags: null,
  actions: {
    resetQuery() {
      this.set('speaker', null);
      this.set('tags', null);
      this.send('refreshRoute');
    }
  }
});
