import Component from '@ember/component';
import { computed } from '@ember/object';
import { and } from '@ember/object/computed';

export default Component.extend({
  hasLocation: and('org.latitude','org.longitude'),
  location: computed('org', function() {
    return [this.get('org.latitude'),this.get('org.longitude')];
  })
});
