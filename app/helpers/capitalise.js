import { helper } from '@ember/component/helper';

export function capitalise(params) {
  return params[0].capitalize();
}

export default helper(capitalise);
