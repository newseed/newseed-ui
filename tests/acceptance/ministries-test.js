import { module, test } from 'qunit';
import { click, visit, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';
import { truncateText } from 'newseed/helpers/truncate-text';

module('Acceptance | ministries', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  hooks.beforeEach(function() {
    this.server.db.emptyData();
    this.server.loadFixtures();
  });

  test('Ministries list page', async function(assert) {
    const ministries = this.server.createList('post', 10, 'ministry');

    await visit('/ministries');

    assert.equal(currentURL(), '/ministries');

    assert.dom('[data-test-ministries-title]').hasText('Ministries');
    assert.dom('[data-test-ministry]').exists({ count: ministries.length });

    ministries.forEach(ministry => {
      assert
        .dom(`[data-test-ministry='${ministry.id}'] [data-test-ministry-link]`)
        .hasText(ministry.title);

      assert
        .dom(`[data-test-ministry='${ministry.id}'] [data-test-ministry-title]`)
        .hasText(ministry.title);

      assert
        .dom(`[data-test-ministry='${ministry.id}'] [data-test-ministry-text]`)
        .hasText(truncateText([ministry.text], { limit: 200, stripHtml: true }));
    });
  });

  test('Ministries list go to ministry page', async function(assert) {
    const ministries = this.server.createList('post', 10, 'ministry');
    const ministry = ministries[0];

    await visit('/ministries');
    await click(`[data-test-ministry='${ministry.id}'] [data-test-ministry-link]`);

    assert.equal(currentURL(), `/ministries/${ministry.id}`);
  });

  test('Ministry page', async function(assert) {
    const ministries = this.server.createList('post', 10, 'ministry');
    const ministry = ministries[0];

    await visit(`/ministries/${ministry.id}`);
    assert.equal(currentURL(), `/ministries/${ministry.id}`);

    assert.dom('[data-test-link-ministries]').exists();
    assert.dom('[data-test-ministry-title]').hasText(ministry.title);
    assert.dom('[data-test-ministry-text]').hasText(ministry.text);
    assert.dom('[data-test-ministry-image]').hasAttribute('src', ministry.image);
    assert.dom('[data-test-ministry-image]').hasAttribute('alt', ministry.title);
  });

  test('Ministry page go to the ministries list', async function(assert) {
    const ministries = this.server.createList('post', 10, 'ministry');
    const ministry = ministries[0];

    await visit(`/ministries/${ministry.id}`);
    assert.equal(currentURL(), `/ministries/${ministry.id}`);

    await click('[data-test-link-ministries]');
    assert.equal(currentURL(), '/ministries');
  });
});
