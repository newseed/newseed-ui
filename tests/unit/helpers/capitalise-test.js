import { capitalise } from 'newseed/helpers/capitalise';
import { module, test } from 'qunit';

module('Unit | Helper | capitalise', function() {
  test('string capitalized', function(assert) {
    assert.equal(capitalise(['test string']), 'Test string');
  });
});
