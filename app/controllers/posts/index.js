import Controller from '@ember/controller';

export default Controller.extend({
  queryParams: ['tags'],
  tags: null,

  actions: {
    resetQuery() {
      this.set('tags', null);
      this.send('refreshRoute');
    }
  }
});
