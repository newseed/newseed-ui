import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  store: service(),
  router: service(),

  actions: {
    saveMeeting(speaker, topic, date) {
      let meeting = this.get('meeting');
      this.store.findRecord('meeting', meeting.id)
        .then(record => {
          record.set('title', topic);
          record.set('author', speaker);
          record.set('date', date);
          record.save();
          this.get('flashMessages').success('Meeting saved to schedule');
        })
        .catch(() => {
          this.get('flashMessages').danger('Something went wrong - meeting not saved');
        });
    },
    saveDate(date) {
      this.set('meeting.date', new Date(date[0]));
    },
    deleteMeeting() {
      let meeting = this.get('meeting');
      this.store.findRecord('meeting', meeting.id, { backgroundReload: false })
        .then(record => {
          record.destroyRecord().then(() => {
            this.get('flashMessages').success('Record successfully deleted');
          })
        })
        .catch(() => {
          this.get('flashMessages').danger('Something went wrong - meeting not deleted');
        });
    }
  }
});
