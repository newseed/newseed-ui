export default function(server) {
  server.loadFixtures();

  server.createList('post', 5);
  server.createList('post', 5, 'slide');
  server.createList('post', 5, 'event');
  server.createList('post', 5, 'ministry');
  server.createList('sermon', 10);
  server.createList('meeting', 5);
}
