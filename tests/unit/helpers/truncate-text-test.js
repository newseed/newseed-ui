import { truncateText } from 'newseed/helpers/truncate-text';
import { module, test } from 'qunit';

module('Unit | Helper | truncate text', function() {
  test('text truncated', function(assert) {
    assert.equal(truncateText(['15 symbols text'], { limit: 5 }), '15 sy...');
  });
  test('html truncated', function(assert) {
    assert.equal(truncateText(['<p>15 &nbsp;symbols text</p>'], { limit: 5 }), '<p>15...');
  });
  test('html striped', function(assert) {
    assert.equal(truncateText(['<p>15 &nbsp;symbols text</p>'], { limit: 5, stripHtml: true }), '15...');
  });
  test('empty text', function(assert) {
    assert.equal(truncateText([''], { limit: 5, stripHtml: true }), '');
  });
  test('null', function(assert) {
    assert.equal(truncateText([null], { limit: 5, stripHtml: true }), '');
  });
});
