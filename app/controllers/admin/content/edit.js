import { computed, get, set } from '@ember/object';
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { A } from '@ember/array';
// import $ from 'jquery';
// import config from 'newseed/config/environment';

export default Controller.extend({
  router: service(),
  stockImages: service(),

  init() {
    this._super(...arguments);
    set(this, 'isSaving', false);
    set(this, 'storageRef', '');
    set(this, 'file', '');
  },

  formatTags: computed('model.tags', function() {
    return get(this, 'model.tags').map(tag => tag.name);
  }),

  actions: {
    titleEdit() {
      set(this, 'editTitle', true);
    },
    saveCat(value) {
      set(this, 'model.type', value);
      this.send('saveContent');
    },
    saveDate(date) {
      set(this, 'model.date', new Date(date));
      this.send('saveContent');
    },
    saveStatus(value) {
      set(this, 'model.status', value);
      this.send('saveContent');
    },
    selectImage(image) {
      set(this, 'model.image', image);
      this.send('saveContent');
    },
    saveExpires(date) {
      set(this, 'model.expiry', new Date(date));
      this.send('saveContent');
    },
    saveFreq(value) {
      set(this, 'model.frequency', value);
      this.send('saveContent');
    },
    saveContent() {
      let model = get(this, 'model');
      set(this, 'isSaving', true);
      if (get(this, 'editTitle')) {
        let title = get(this, 'model.title');
        model.set('title', title);
        set(this, 'editTitle', false);
      }
      model.save()
        .then(() => {
          set(this, 'isSaving', false);
          get(this, 'flashMessages').success('Content successfully saved');
        })
        .catch(() => {
          set(this, 'isSaving', false);
          get(this, 'flashMessages').danger('Something went wrong - content not saved');
        });
    },
    deleteContent() {
      let model = get(this, 'model');
      model.destroyRecord()
        .then(() => {
          get(this, 'router').transitionTo('admin.content');
        })
        .catch(() => {
          get(this, 'flashMessages').danger('Something went wrong - content not deleted');
        });
    },
    // didSelectFile(files, resetInput) {
      // let reader = new FileReader();
      // reader.onloadend = run.bind(this, function() {
      //   var dataURL = reader.result;
      //   var output = document.getElementById('output');
      //   output.src = dataURL;
      //   set(this, 'file', files[0]);
      // });
      // reader.readAsDataURL(files[0])
      // let date = new Date().toGMTString();
      // set(this, 'file', files[0]);
      // let path = `media/${get(this, 'file.name')}`;
      // let formData = new FormData();
      // formData.append('file', files[0]);
      // $.ajax({
      //     'type': 'PUT',
      //     'url': `https://s3-${config.aws.region}.amazonaws.com/${config.aws.bucket}/${path}`,
      //     'datatype': 'xml',
      //     'processData': false,
      //     'contentType': false,
      //     'crossDomain': true,
      //     'Authorization': '962859d727c7fcb476d486dd3b35d6b3675fd9763db1d6e71b12bad5799fa6f1',
      //     'Date': date,
      //     'x-amz-date': date,
      //     'data': formData
      // }).done(data => {
      //     console.log('SUCCESS', data);
      // }).fail(err => {
      //     console.log('FAIL', err);
      // });
      // () => {
      //   get(this, 'flashMessages').danger('Something went wrong - file not uploaded')
      // },
      // () => {
      //   set(this, 'uploadProgress', null);
      //   set(this, 'model.type', 'sermon');
      //   uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
      //     set(this, 'model.link', downloadURL);
      //     this.send('saveContent');
      //     resetInput();
      //   });
      // });
    // },
    addTag(tag) {
      const fullTag = { name: tag }
      if (!get(this, 'model.tags')) {
        set(this, 'model.tags', A());
      }
      get(this, 'model.tags').pushObject(fullTag);
      this.send('saveContent');
    },
    removeTagAtIndex(index) {
      get(this, 'model.tags').removeAt(index);
      this.send('saveContent');
    }
  }
});
