import Route from '@ember/routing/route';
import SetChurchController from 'newseed/mixins/org-set-controller';

import { inject as service } from '@ember/service';

import { setQuery } from 'newseed/utils/set-query';

export default Route.extend(SetChurchController, {
  infinity: service(),
  queryParams: {
    tags: {
      refreshModel: true
    }
  },
  model(params) {
    const fullQuery = setQuery(
      {
        type: ['event', 'news', 'need'],
        status: 'published',
        perPage: 20,
        startingPage: 1,
        countParam: 'meta.count',
      }, 
      params
    );
    return this.infinity.model('post', fullQuery);
  },
  actions: {
    refreshRoute() {
      this.refresh();
    }
  }
});
