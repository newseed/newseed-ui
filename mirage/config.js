export default function() {
  this.urlPrefix = 'http://localhost:8081';
  this.namespace = '/api/v1';
  this.timing = 400;

  this.post('auth/login', (schema, request) => {
    const email = request.params.username;
    const password = request.params.password;
    const access_token = 123;

    return { email, password, access_token };
  });

  this.resource('meeting');
  this.resource('org');
  this.resource('post');
  this.resource('sermon');

  this.get('orgs/:id', (schema) => {
    return schema.orgs.first();
  });
}
