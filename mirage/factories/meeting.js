import { Factory } from 'ember-cli-mirage';
import faker from 'faker';
import moment from 'moment';

export default Factory.extend({
  title: () => faker.lorem.sentence(),
  speaker: () => faker.name.findName(),
  date: () => faker.date.between(moment(), moment().endOf('week').add(1, 'day'))
});
