import { module, test } from 'qunit';
import { visit } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';


module('Acceptance | main-page', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  hooks.beforeEach(function() {
    this.server.db.emptyData();
    this.server.loadFixtures();
  });

  test('it shows index link with org name', async function(assert) {
    const org = this.server.schema.orgs.first();

    await visit('/');

    assert.dom('[data-test-header-link-index]').hasText(org.name);
  });

  test('it does not show content links', async function(assert) {
    await visit('/');

    assert.dom('[data-test-header-link-notices]').doesNotExist();
    assert.dom('[data-test-header-link-ministries]').doesNotExist();
    assert.dom('[data-test-header-link-messages]').doesNotExist();
    assert.dom('[data-test-header-link-page]').doesNotExist();
  });

  test('it shows contact link', async function(assert) {
    await visit('/');

    assert.dom('[data-test-header-link-contact]').exists();
  });

  test('it shows notices link', async function(assert) {
    this.server.createList('post', 2, 'event');
    await visit('/');

    assert.dom('[data-test-header-link-notices]').exists();
  });

  test('it shows ministries link', async function(assert) {
    this.server.createList('post', 2, 'ministry');
    await visit('/');

    assert.dom('[data-test-header-link-ministries]').exists();
  });

  test('it shows messages link', async function(assert) {
    this.server.createList('sermon', 2);
    await visit('/');

    assert.dom('[data-test-header-link-messages]').exists();
  });

  test('it shows page links', async function(assert) {
    const pages = this.server.createList('post', 2, 'page');
    await visit('/');

    pages.forEach(page => {
      assert.dom(`[data-test-header-link-page='${page.id}']`).hasText(page.title);
    });
  });
});
