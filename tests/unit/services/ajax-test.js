import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Service | ajax', function(hooks) {
  setupTest(hooks);

  test('empty headers with no auth data', function(assert) {
    const ajaxService = this.owner.lookup('service:ajax');
    assert.equal(JSON.stringify(ajaxService.get('headers')), '{}');
  });
  test('not empty headers with auth data', function(assert) {
    const sessionService = this.owner.lookup('service:session');
    const ajaxService = this.owner.lookup('service:ajax');
    const expected = JSON.stringify({ "authorization": "Bearer token123" });

    sessionService.set('data.authenticated.access_token', 'token123');

    assert.equal(JSON.stringify(ajaxService.get('headers')), expected);
  });
});
