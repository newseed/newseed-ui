import Route from '@ember/routing/route';
import RSVP from 'rsvp';
import SetChurchController from 'newseed/mixins/org-set-controller';

export default Route.extend(SetChurchController, {
  model() {
    return RSVP.hash({
      posts: this.store.query('post', { status: 'published' }),
      sermons: this.store.query('sermon', { status: 'published' }),
      meetings: this.store.findAll('meeting')
    });
  },
});
