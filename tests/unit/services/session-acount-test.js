import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import sinon from 'sinon';

module('Unit | Service | ajax', function(hooks) {
  setupTest(hooks);

  test('it does not load the user when unauthenticated', async function(assert) {
    const storeService = this.owner.lookup('service:store');
    const sessionAccountService = this.owner.lookup('service:session-account');

    sinon.replace(storeService, 'find', sinon.fake());

    await sessionAccountService.loadCurrentUser();

    assert.notOk(sessionAccountService.get('account'));

    assert.ok(storeService.find.notCalled);

    sinon.restore();
  });

  test('it loads and set the user when authenticated', async function(assert) {
    const storeService = this.owner.lookup('service:store');
    const sessionService = this.owner.lookup('service:session');
    const sessionAccountService = this.owner.lookup('service:session-account');

    const fakeResponse = { name: 'test' };

    sinon.replace(storeService, 'find', sinon.fake.resolves(fakeResponse));

    sessionService.set('data.authenticated.account', { id: 1 });

    await sessionAccountService.loadCurrentUser();

    assert.equal(JSON.stringify(sessionAccountService.get('account')), JSON.stringify(fakeResponse));

    assert.ok(storeService.find.calledOnce);
    assert.ok(storeService.find.calledWith('account', 1));

    sinon.restore();
  });
});
