import { module, test } from 'qunit';
import { visit, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';


module('Acceptance | contact-page', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  hooks.beforeEach(function() {
    this.server.db.emptyData();
    this.server.loadFixtures();
  });

  test('it displays correctly', async function(assert) {
    const org = this.server.schema.orgs.first();
    await visit('/contact');

    assert.equal(currentURL(), '/contact');
    assert.dom('[data-test-contact-title]').exists();
    assert.dom('[data-test-contact-form]').exists();
    assert.dom('[data-test-contact-form]').hasAttribute('action', `https://formspree.io/${org.email}`);
    assert.dom('[data-test-contact-form]').hasAttribute('method', 'POST');
    assert.dom('[data-test-contact-form] [data-test-input-name]').exists();
    assert.dom('[data-test-contact-form] [data-test-input-email]').exists();
    assert.dom('[data-test-contact-form] [data-test-input-subject]').exists();
    assert.dom('[data-test-contact-form] [data-test-input-message]').exists();
    assert.dom('[data-test-contact-form] [data-test-button-submit]').exists();
  });
});
