import Controller from '@ember/controller';
import { get, computed } from '@ember/object';
import moment from 'moment';
import EmberObject from '@ember/object';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';

export default Controller.extend(UnauthenticatedRouteMixin, {
  slides: computed('model', function() {
    return get(this, 'model.posts').filter(post => {
      let tags = get(post, 'tags');
      return tags.filter(tag => tag.name === 'slide').length;
    });
  }),
  events: computed('model', function() {
    return get(this, 'model.posts').filter(post => {
      return get(post, 'type') === 'event';
    });
  }),
  notices: computed('model', function() {
    return get(this, 'model.posts')
      .filter(post => {
        let tags = get(post, 'tags');
        let tagArr = tags.map(tag => tag.name);
        if (tagArr.includes('slide')) {
          return false;
        }
        return ['news', 'event', 'need'].includes(get(post, 'type'));
      })
      .sort((a, b) => {
        return moment(get(a, 'date')).isBefore(moment(get(b, 'date'))) ? 1 : -1;
      })
      .slice(0, 7);
  }),
  media: computed('model.sermons', function() {
    return get(this, 'model.sermons')
      .toArray()
      .sort((a, b) => {
        return moment(get(a ,'date')).isBefore(moment(get(b, 'date'))) ? 1 : -1;
      })
      .slice(0, 5);
  }),
  nextMeeting: computed('model.meetings', 'org', function() {
    let now = moment();
    let display = EmberObject.create();
        // set the checkDate to next Sunday just before midnight
    let checkDate = moment().day(7).endOf('day');
    let meetings = get(this, 'model.meetings').filter(meeting => {
      let date = get(meeting, 'date');
      // Meeting date needs to be after time now, and same or before the checkDate
      return moment(date).isAfter(now, 'hour') && moment(date).isSameOrBefore(checkDate, 'minute');
    });
    if (meetings.length) {
      return meetings[0];
    } else if (get(this, 'org')) {
      // Set date for coming Sunday
      let date = moment().day(7).startOf('day');
      let timesArray = get(this, 'org.meetingTimes');

      if (timesArray.length) {
        const meetingDay = get(this, 'org.meetingDay');
        const day = meetingDay ? moment(moment().day(meetingDay)).weekday() : 7;
        // Split time with any matching seperator
        let hourMinuteArray = timesArray[0].split(new RegExp(/[:,.|-]/g));
        // Convert first time to decimal and add to Sunday
        date.set('day', parseFloat(day));
        date.set('hour', parseFloat(hourMinuteArray[0]));
        date.set('minute', parseFloat(hourMinuteArray[1]));
        display.set('date', date.toJSON());
      }
    }
    return display;
  }),
});
