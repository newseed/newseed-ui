import Controller from '@ember/controller';
import { computed } from '@ember/object';
import moment from 'moment';

const currentSunday = moment().day(0).endOf('day');

export default Controller.extend({
  init() {
    this._super(...arguments);
    this.set('isSaving', false);
  },
  upcoming: computed('model.@each', function () {
    return this.get('model')
      .filter(item => {
        return moment(item.get('date')).isSameOrAfter(currentSunday);
      })
      .sort((a, b) => {
        return moment(a.get('date')).isAfter(moment(b.get('date'))) ? 1 : -1;
      });
  }),
  actions: {
    saveModel() {
      let model = this.get('model');
      this.set('isSaving', true);
      model.save()
        .then(() => {
          this.set('isSaving', false);
          this.get('flashMessages').success('Content successfully saved')
        })
        .catch(() => {
          this.set('isSaving', false);
          this.get('flashMessages').danger('Something went wrong - content not saved')
        });
    },
    addMeeting() {
      const store = this.store;
      const times = this.get('org.meetingTimes');
      const defaultTime = "10:30";
      const momentTime = times.length ? moment(times[0], 'h:mm') : moment(defaultTime, 'HH:mm:ss');

      let meeting = store.createRecord('meeting', { date: momentTime.day(7).toDate() });
      meeting.save().then(() => {
        this.send('refreshRoute');
      }).catch(() => {
        this.get('flashMessages').danger('Something went wrong - meeting not created');
        meeting.deleteRecord();
      });
    }
  }
});
