import config from 'newseed/config/environment';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import AjaxService from 'ember-ajax/services/ajax';

export default AjaxService.extend({
  session: service(),

  host: config.apiHost,
  namespace: config.apiNameSpace,
  // trustedHosts: [/\.example\./, 'foo.bar.com'],

  headers: computed('session.authToken', {
    get() {
      let headers = {};
      const token = this.get('session.data.authenticated.access_token');
      if (token) {
        headers['authorization'] = `Bearer ${token}`;
      }
      return headers;
    }
  })
});
