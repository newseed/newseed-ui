export default {
    "address": "7778 Leopold Flat",
    "address2": "Suite 878",
    "city": "Kamronville",
    "country": "Kyrgyz Republic",
    "email": "Aliyah70@hotmail.com",
    "facebook": "http://facebook.com/carter_group",
    "latitude": "-69.6187",
    "longitude": "-117.6714",
    "name": "Carter Group",
    "phone": "469-813-7668",
    "meetingDay": "Sunday",
    "meetingTimes": [
        "9:00",
        "10:30"
    ],
    "state": "Wisconsin",
    "twitter": "http://twitter.com/carter_group",
    "zip": "21675-4934"
};
