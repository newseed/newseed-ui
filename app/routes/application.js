import { inject as service } from '@ember/service'
import Route from '@ember/routing/route';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
import config from 'newseed/config/environment';
import _ from 'lodash';

export default Route.extend(ApplicationRouteMixin, {

  sessionAccount: service('session-account'),
  headData: service(),
  moment: service(),

  beforeModel() {
    this.get('moment').setTimeZone('Pacific/Auckland');
    return this._loadCurrentUser();
  },

  sessionAuthenticated() {
    this._super(...arguments);
    this._loadCurrentUser();
  },

  _loadCurrentUser() {
    return this.get('sessionAccount').loadCurrentUser().catch(() => this.get('session').invalidate());
  },

  afterModel() {
    const header = this.headData;
    const keys = _.keys(config.header);    
    keys.forEach(key => {
      header.set(key, config.header[key]);
    });
  }
});
