import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  contactEmail: computed('org.email', function() {
    return this.get('org.email');
  }),
});
