import { computed, get } from '@ember/object';
import { inject as service } from '@ember/service';
import { pluralize } from 'ember-inflector';
import { A } from '@ember/array';
import Controller from '@ember/controller';
import moment from 'moment';

export default Controller.extend({
  session: service(),

  init() {
    this._super(...arguments);
    this.set('showingItem', { type:'all', label:'All content' });
  },

  items: computed('model', function() {
    let seen = {},
        items = A();
    const updater = (type) => {
      seen[type] = true;
      items.push(this._setShowing(type))
    }
    get(this, 'model').forEach((post) => {
      let type = post.get('type');
      return seen.hasOwnProperty(type)
        ? false
        : updater(type);
    });
    items.splice(0, 0, { type:'all', label:'All content' });
    return items;
  }),

  itemHeader: computed('showingItem', function() {
    return get(this, 'showingItem').label;
  }),

  contents: computed('model', 'showingItem', function() {
    let model = get(this, 'model');
    return model
      // filter the objects by select value
      .filter(content => {
        let showing = get(this, 'showingItem');
        if (showing.type === 'all') {
          return true;
        } else if (showing.type === get(content, 'type')) {
          return true;
        }
      })
      // sort the objects by date descending
      .sort((a, b) => {
        const aDate = get(a, 'updated_at') ? get(a, 'updated_at') : get(a, 'date');
        const bDate = get(b, 'updated_at') ? get(b, 'updated_at') : get(b, 'date');
        return moment(aDate).isBefore(moment(bDate)) ? 1 : -1;
      });
  }),

  _setShowing(type) {
    return { type, label: type === 'all' ? 'All content' : pluralize(type).capitalize() }
  },

  actions: {
    filterModels(value) {
      this.set('showingItem', this._setShowing(value));
    },
    createContent(postType) {
      let store = get(this, 'store');
      let record = store.createRecord('post', {
        title: `New ${postType.capitalize()} Content`,
        type: postType,
        author: get(this, 'session.data.authenticated.user.id')
      });
      record.save().then(rec => {
        let id = rec.get('id');
        this.transitionToRoute('admin.content.edit', id);
      });
    }
  }
});
