import EmberRouter from '@ember/routing/router';
import config from './config/environment';
import { get } from '@ember/object';
import { inject as service } from '@ember/service';
import { scheduleOnce } from '@ember/runloop';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL,

  metrics: service(),

  init() {
    this._super(...arguments);

    this.on('routeDidChange', () => {
      scheduleOnce('afterRender', this, this._trackPage);
    });
  },

  _trackPage() {
    const page = this.get('url');
    const title = this.getWithDefault('currentRouteName', 'unknown');

    get(this, 'metrics').trackPage({ page, title });
  }
});

Router.map(function() {
  this.route('messages');
  this.route('posts', { path: 'news' }, function() {
    this.route('show', { path: ':id' });
  });
  this.route('page', { path: ':id' });
  this.route('contact');
  this.route('ministries', function() {
    this.route('ministry', { path: ':id' });
  });
  this.route('login');
  this.route('signup');
  this.route('forgot-password');
  this.route('admin', function() {
    this.route('settings');
    this.route('edit', { path: 'edit/:id' });
    this.route('content', function() {
      this.route('edit', { path: ':id' });
    });
    this.route('message', function() {
      this.route('edit', { path: ':id' });
    });
    this.route('meetings');
    this.route('people');
  });
});

export default Router;
