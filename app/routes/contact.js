import Route from '@ember/routing/route';
import SetChurchController from 'newseed/mixins/org-set-controller';

export default Route.extend(SetChurchController, {
});
