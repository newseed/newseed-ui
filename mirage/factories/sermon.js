import { Factory } from 'ember-cli-mirage';
import faker from 'faker';
import moment from 'moment';

export default Factory.extend({
  title: () => faker.lorem.words(),
  date: () => faker.date.between(moment().startOf('week'), moment().endOf('week')),
  text: () => faker.lorem.paragraph(),
  author: () => faker.name.findName(),
  speaker: () => faker.name.findName(),
  image: () => faker.random.image(),
  fileUrl: () => faker.random.image(),
  fileSize: () => faker.random.number({ min: 100, max: 700 }),
  status: 'draft',
  tags: () => [{ name: 'eee' }, { name: 'two' }]
});
