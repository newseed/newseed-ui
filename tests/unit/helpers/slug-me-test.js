import { slugMe } from 'newseed/helpers/slug-me';
import { module, test } from 'qunit';

module('Unit | Helper | slug-me', function() {
  test('string sluged', function(assert) {
    assert.equal(slugMe(['test string']), 'test-string');
    assert.equal(slugMe(['  1 test @ # $ % ^ &string --- 1  ']), '1-test-string-1');
  });
});
